#ifndef ROOM_H
#define ROOM_H

#include <iostream>
#include <string>

#include "Helper.h"

using namespace std;
static int currentRoomID = 0;

class Room;

class RoomProto{

public:
	RoomProto(string roomDescription, int exitNorth, int exitEast, int exitSouth, int exitWest);
	virtual void look(Room* rms[], word *dir, noun *nns) = 0;
	string description;
	int exits_to_room[4];
	int roomID;
};

class Room : public RoomProto
{
public:
	Room(string roomDescription, int exitNorth, int exitEast, int exitSouth, int exitWest) : RoomProto(roomDescription, exitNorth, exitEast, exitSouth, exitWest){}
	void look(Room* rms[], word *dir, noun *nns);
};

class DarkRoom : public Room
{
public:
	DarkRoom(string roomDescription, int exitNorth, int exitEast, int exitSouth, int exitWest) : Room(roomDescription, exitNorth, exitEast, exitSouth, exitWest) {}
	void look(Room* rms[], word *dir, noun *nns);

private:

};

#endif // !Room
