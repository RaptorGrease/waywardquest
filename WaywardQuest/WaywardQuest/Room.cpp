#include "Room.h"

RoomProto::RoomProto(string roomDescription, int exitNorth, int exitEast, int exitSouth, int exitWest)
{
	description.assign(roomDescription);
	exits_to_room[0] = exitNorth;
	exits_to_room[1] = exitEast;
	exits_to_room[2] = exitSouth;
	exits_to_room[3] = exitWest;
	roomID = currentRoomID++;
}

void Room::look(Room* rms[], word * dir, noun * nns)
{
	cout << "I am in a " << rms[location]->description << endl;
	for (int i = 0; i < 4; i++)
	{
		if (exits_to_room[i] != -1)
		{
			cout << "There is an exit " << dir[i].text << " to a " << rms[exits_to_room[i]]->description << "." << endl;
		}
	}
	for (int i = 0; i < 6; i++)
	{
		if (nns[i].location == roomID)
		{
			cout << "I see a " << nns[i].description << "." << endl;
		}
	}
}

void DarkRoom::look(Room* rms[], word * dir, noun * nns)
{
	if (inventory.find(nns[TORCH]) != inventory.end())
	{
		Room::look(rms, dir, nns);
	}
	else
	{
		cout << "the room is too dark to see anything" << endl;
	}
}
