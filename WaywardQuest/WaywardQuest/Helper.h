#ifndef HELPER_H
#define HELPER_H

#include <string>
#include <iostream>
#include <map>

using namespace std;

enum en_DIRS { NORTH, EAST, SOUTH, WEST };
enum en_ROOMS { ROOM0, ROOM1, ROOM2, ROOM3, ROOM4, ROOM5, ROOM6, ROOM7, ROOM8, ROOM9, ROOM10 };
enum en_VERBS { GET, DROP, USE, OPEN, CLOSE, EXAMINE, INVENTORY, LOOK };
enum en_NOUNS { DOOR, KEY, TORCH, ITEM3, ITEM4, ITEM5 };

struct noun
{
	string label;
	string description;
	int code;
	int location;
	bool can_carry;
	friend bool operator<(const noun& l, const noun& r) { return l.label < r.label; }
};

struct word
{
	int code;
	string text;
};

static int location = ROOM0; // using the enumerated type identifier

static int inv[] = {0, 0, 0, 0, 0, 0};
static map <noun, int> testInv;
static map <noun, int> inventory;

#endif // !HELPER_H
