
#include <iostream>
#include <string>
#include <vector>
#include <cctype>
#include <map>
#include "Room.h"
#include "Helper.h"



const int NONE = -1;
const int DIRS = 4;
const int ROOMS = 11;
const int VERBS = 8;
const int NOUNS = 6;
using namespace std;



struct room
{
	string description;
	int exits_to_room[DIRS];
};



//void set_rooms(room* rms);
void section_command(string inpt, string &wrd1, string &wrd2);
void set_directions(word *dir);
void set_verbs(word *vbs);
void look_around(int loc, Room* rms[], word *dir, noun *nns);
bool parser(int &loc, string wd1, string wd2, word *dir, word *vbs, Room* rms[], noun *nns);
void set_nouns(noun *nns);

int main() {
	Room* room0 = new DarkRoom("Altar room", NONE, NONE, ROOM2, NONE);
	Room* room1 = new Room("Room1", NONE, NONE, ROOM3, NONE);
	Room* room2 = new Room("Room2", ROOM0, ROOM3, NONE, NONE);
	Room* room3 = new Room("Room3", ROOM1, ROOM4, ROOM5, ROOM2);
	Room* room4 = new Room("Room4", NONE, NONE, NONE, ROOM3);
	Room* room5 = new Room("Room5", ROOM3, NONE, ROOM8, NONE);
	Room* room6 = new Room("Room6", NONE, NONE, NONE, NONE);
	Room* room7 = new Room("Room7", NONE, ROOM8, ROOM10, NONE);
	Room* room8 = new Room("Room8", ROOM5, ROOM9, NONE, ROOM7);
	Room* room9 = new Room("Room9", NONE, NONE, NONE, ROOM8);
	Room* room10 = new Room("Room10", ROOM7, NONE, NONE, NONE);

	string command = "";
	string word1;
	string word2;
	Room* rooms[] = { room0,room1,room2,room3,room4,room5,room6,room7,room8,room9,room10 };
	//room rooms[ROOMS];
	//set_rooms(rooms);
	word directions[DIRS];
	set_directions(directions);
	word verbs[VERBS];
	set_verbs(verbs);
	noun nouns[NOUNS];
	set_nouns(nouns);

	cout << "<Reserved for intro to story>" << endl;
	cout << "\nYour obvious choices here are to go west, north, south or east. Looking and using objects wont hurt either." << endl;

	while (word1 != "QUIT") {
		command.clear();
		getline(cin, command);
		word1.clear();
		word2.clear();
		section_command(command, word1, word2);
		parser(location, word1, word2, directions, verbs, rooms, nouns);
		//cout << word1 << " " << word2 << endl;
	}

	return 0;
}

void set_verbs(word *vbs) {
	// Reminder GET, DROP, USE, OPEN, CLOSE, EXAMINE, INVENTORY, LOOK
	vbs[GET].code = GET;
	vbs[GET].text = "GET";
	vbs[DROP].code = DROP;
	vbs[DROP].text = "DROP";
	vbs[USE].code = USE;
	vbs[USE].text = "USE";
	vbs[OPEN].code = OPEN;
	vbs[OPEN].text = "OPEN";
	vbs[CLOSE].code = CLOSE;
	vbs[CLOSE].text = "CLOSE";
	vbs[EXAMINE].code = EXAMINE;
	vbs[EXAMINE].text = "EXAMINE";
	vbs[INVENTORY].code = INVENTORY;
	vbs[INVENTORY].text = "INVENTORY";
	vbs[LOOK].code = LOOK;
	vbs[LOOK].text = "LOOK";
}

void section_command(string Cmd, string &wrd1, string &wrd2) {
	string sub_str;
	vector<string> words;
	char search = ' ';
	size_t i, j;

	for (i = 0; i < Cmd.size(); i++) {
		if (Cmd.at(i) != search)
		{
			sub_str.insert(sub_str.end(), Cmd.at(i));
		}
		if (i == Cmd.size() - 1) {
			words.push_back(sub_str);
			sub_str.clear();
		}
		if (Cmd.at(i) == search)
		{
			words.push_back(sub_str);
			sub_str.clear();
		}
	}

	if (words.size() > 0)
	{
		for (i = words.size() - 1; i > 0; i--)
		{
			if (words.at(i) == "")
			{
				words.erase(words.begin() + 1);
			}
		}
	}

	for (i = 0; i < words.size(); i++)
	{
		for (j = 0; j < words.at(i).size(); j++)
		{
			if (islower(words.at(i).at(j)))
			{
				words.at(i).at(j) = toupper(words.at(i).at(j));
			}
		}
	}

	if (words.size() == 0)
	{
		cout << "No command given" << endl;
	}
	if (words.size() == 1)
	{
		wrd1 = words.at(0);
	}
	if (words.size() == 2)
	{
		wrd1 = words.at(0);
		wrd2 = words.at(1);
	}
	if (words.size() > 2)
	{
		cout << "Ye command is too long! Only type one or two words (direction or verb and noun)" << endl;
	}

}

void look_around(int loc, Room* rms[], word *dir, noun *nns) {


	rms[loc]->look(rms, dir, nns);

	//LOOK should allow players to see what exits exist from current room
	/*
	int i;
	for (i = 0; i < DIRS; i++)
	{
		if (rms[loc].exits_to_room[i] != NONE)
		{
			cout << "There is an exit " << dir[i].text << " to a " << rms[rms[loc].exits_to_room[i]].description << "." << endl;
		}
	}
	for (i = 0; i < NOUNS; i++)
	{
		if (nns[i].location == loc)
		{
			cout << "I see a " << nns[i].description << "." << endl;
		}
	}*/
}

bool parser(int &loc, string wd1, string wd2, word *dir, word *vbs, Room* rms[], noun *nns) {
	int i;
	int VERB_ACTION = NONE;
	int NOUN_MATCH = NONE;
	static bool door_state = false;
	for (i = 0; i < DIRS; i++)
	{
		if (wd1 == dir[i].text)
		{
			if (rms[loc]->exits_to_room[dir[i].code] != NONE)
			{
				loc = rms[loc]->exits_to_room[dir[i].code];
				cout << "I am now in a " << rms[loc]->description << "." << endl;
				return true;
			}
			else
			{
				cout << "No exit that way" << endl;
				return true;
			}
		}
	}
	for (i = 0; i < VERBS; i++)
	{
		if (wd1 == vbs[i].text) {
			VERB_ACTION = vbs[i].code;
			break;
		}
	}
	if (wd2 != "")
	{
		for (i = 0; i < NOUNS; i++)
		{
			if (wd2 == nns[i].label)
			{
				NOUN_MATCH = nns[i].code;
				break;
			}
		}
	}

	/*
	if (VERB_ACTION == NONE)
	{
		cout << "No valid command entered." << endl;
		return true;
	}
	*/
	if (VERB_ACTION == LOOK)
	{
		look_around(loc, rms, dir, nns);
	}
	if (VERB_ACTION == OPEN) {
		if (NOUN_MATCH == DOOR) {
			if (loc == ROOM5 || loc == ROOM6) {
				if (door_state == false) {
					if (inventory.find(nns[KEY]) != inventory.end()) {
						door_state = true;
						rms[ROOM5]->exits_to_room[EAST] = ROOM6;
						rms[ROOM6]->exits_to_room[WEST] = ROOM5;
						nns[DOOR].description.clear();
						nns[DOOR].description.assign("an open doorway to a dark room");
						cout << "I have opened the door." << endl;
						return true;
					}
					else
					{
						cout << "It would seem that you need a key" << endl;
						return true;
					}
				}
				else if (door_state == true) {
					cout << "The door is already open." << endl;
					return true;
				}
			}
			else
			{
				cout << "There is no door to open here." << endl;
				return true;
			}
		}
		else
		{
			cout << "Opening that is not possible." << endl;
			return true;
		}
	}
	if (VERB_ACTION == GET)
	{
		switch (NOUN_MATCH)
		{
		case DOOR:
			break;
		case KEY:
			if (loc == ROOM1)
			{
				if (nns[KEY].can_carry == true)
				{
					if (inventory.find(nns[KEY]) == inventory.end())
					{
						cout << "You picked up a " << nns[KEY].label << endl;
						inventory[nns[KEY]] = 1;
						nns[KEY].location = NONE;
						return true;
					}
				}
			}
			break;
		case TORCH:
			if (loc == ROOM8 || loc == ROOM0)
			{
				if (nns[TORCH].can_carry == true)
				{
					if (inventory.find(nns[TORCH]) == inventory.end())
					{
						cout << "You picked up a " << nns[TORCH].label << endl;
						inventory[nns[TORCH]] = 1;
						nns[TORCH].location = NONE;
						testInv[nns[TORCH]] = 1;
						inv[TORCH] = 1;
						return true;
					}
				}
			}
			break;
		case ITEM3:
			break;
		case ITEM4:
			break;
		case ITEM5:
			break;
		default:
			break;
		}
	}

	if (VERB_ACTION == INVENTORY)
	{
		if (inventory.empty())
		{
			cout << "Inventory is empty" << endl;
			return true;
		}
		cout << "Inventory contains:" << endl;
		for (auto& obj : inventory)
		{
			noun itm = obj.first;
			cout << itm.label << ": " << itm.description << endl;
		}
		return true;
	}
	if (VERB_ACTION == CLOSE)
	{
		if (NOUN_MATCH == DOOR)
		{
			if (loc == ROOM5 || loc == ROOM6) {
				if (door_state == true)
				{
					door_state = false;
					rms[ROOM5]->exits_to_room[EAST] = NONE;
					rms[ROOM6]->exits_to_room[WEST] = NONE;
					nns[DOOR].description.clear();
					nns[DOOR].description.assign("a closed door with a pentagram shaped lock");
					cout << "I have closed the door." << endl;
					return true;
				}
				else if (door_state == true) {
					cout << "The door is already closed." << endl;
					return true;
				}
			}
			else
			{
				cout << "There is no door to close here" << endl;
			}
		}
		else
		{
			cout << "Closing that is not possible." << endl;
			return true;
		}
	}
	return false;
}

void set_nouns(noun *nns) {

	nns[DOOR].label = "DOOR";
	nns[DOOR].code = DOOR;
	nns[DOOR].description = "a closed door with a pentagram shaped lock";
	nns[DOOR].can_carry = false;
	nns[DOOR].location = ROOM5;

	nns[KEY].label = "KEY";
	nns[KEY].code = KEY;
	nns[KEY].description = "a pentagram shaped key";
	nns[KEY].can_carry = true;
	nns[KEY].location = ROOM1;

	nns[TORCH].label = "TORCH";
	nns[TORCH].code = TORCH;
	nns[TORCH].description = "a blazing torch";
	nns[TORCH].can_carry = true;
	nns[TORCH].location = ROOM0;

	nns[ITEM3].label = "ITEM3";
	nns[ITEM3].code = ITEM3;
	nns[ITEM3].description = "UNDEFINED OBJECT";
	nns[ITEM3].can_carry = true;
	nns[ITEM3].location = ROOM10;

	nns[ITEM4].label = "ITEM4";
	nns[ITEM4].code = ITEM4;
	nns[ITEM4].description = "UNDEFINED OBJECT";
	nns[ITEM4].can_carry = false;
	nns[ITEM4].location = ROOM9;

	nns[ITEM5].label = "ITEM5";
	nns[ITEM5].code = ITEM5;
	nns[ITEM5].description = "UNDEFINED OBJECT";
	nns[ITEM5].can_carry = false;
	nns[ITEM5].location = ROOM4;

}
void set_directions(word *dir) {
	dir[NORTH].code = NORTH;
	dir[NORTH].text = "NORTH";
	dir[EAST].code = EAST;
	dir[EAST].text = "EAST";
	dir[SOUTH].code = SOUTH;
	dir[SOUTH].text = "SOUTH";
	dir[WEST].code = WEST;
	dir[WEST].text = "WEST";
}
/*
void set_rooms(room* rms) {
	rms[ROOM0].description.assign("Place holder for room 0");
	rms[ROOM0].exits_to_room[NORTH] = NONE;
	rms[ROOM0].exits_to_room[EAST] = NONE;
	rms[ROOM0].exits_to_room[SOUTH] = ROOM2;
	rms[ROOM0].exits_to_room[WEST] = NONE;

	rms[ROOM1].description.assign("Place holder for room 1");
	rms[ROOM1].exits_to_room[NORTH] = NONE;
	rms[ROOM1].exits_to_room[EAST] = NONE;
	rms[ROOM1].exits_to_room[SOUTH] = ROOM3;
	rms[ROOM1].exits_to_room[WEST] = NONE;

	rms[ROOM2].description.assign("Place holder for room 2");
	rms[ROOM2].exits_to_room[NORTH] = ROOM0;
	rms[ROOM2].exits_to_room[EAST] = ROOM3;
	rms[ROOM2].exits_to_room[SOUTH] = NONE;
	rms[ROOM2].exits_to_room[WEST] = NONE;

	rms[ROOM3].description.assign("Place holder for room 3");
	rms[ROOM3].exits_to_room[NORTH] = ROOM1;
	rms[ROOM3].exits_to_room[EAST] = ROOM4;
	rms[ROOM3].exits_to_room[SOUTH] = ROOM5;
	rms[ROOM3].exits_to_room[WEST] = ROOM2;

	rms[ROOM4].description.assign("Place holder for room 4");
	rms[ROOM4].exits_to_room[NORTH] = NONE;
	rms[ROOM4].exits_to_room[EAST] = NONE;
	rms[ROOM4].exits_to_room[SOUTH] = NONE;
	rms[ROOM4].exits_to_room[WEST] = ROOM3;

	rms[ROOM5].description.assign("Place holder for room 5");
	rms[ROOM5].exits_to_room[NORTH] = ROOM3;
	rms[ROOM5].exits_to_room[EAST] = NONE;
	rms[ROOM5].exits_to_room[SOUTH] = ROOM8;
	rms[ROOM5].exits_to_room[WEST] = NONE;

	rms[ROOM6].description.assign("Place holder for room 6");
	rms[ROOM6].exits_to_room[NORTH] = NONE;
	rms[ROOM6].exits_to_room[EAST] = NONE;
	rms[ROOM6].exits_to_room[SOUTH] = NONE;
	rms[ROOM6].exits_to_room[WEST] = NONE;

	rms[ROOM7].description.assign("Place holder for room 7");
	rms[ROOM7].exits_to_room[NORTH] = NONE;
	rms[ROOM7].exits_to_room[EAST] = ROOM8;
	rms[ROOM7].exits_to_room[SOUTH] = ROOM10;
	rms[ROOM7].exits_to_room[WEST] = NONE;

	rms[ROOM8].description.assign("Place holder for room 8");
	rms[ROOM8].exits_to_room[NORTH] = ROOM5;
	rms[ROOM8].exits_to_room[EAST] = ROOM9;
	rms[ROOM8].exits_to_room[SOUTH] = NONE;
	rms[ROOM8].exits_to_room[WEST] = ROOM7;

	rms[ROOM9].description.assign("Place holder for room 9");
	rms[ROOM9].exits_to_room[NORTH] = NONE;
	rms[ROOM9].exits_to_room[EAST] = NONE;
	rms[ROOM9].exits_to_room[SOUTH] = NONE;
	rms[ROOM9].exits_to_room[WEST] = ROOM8;

	rms[ROOM10].description.assign("Place holder for room 10");
	rms[ROOM10].exits_to_room[NORTH] = ROOM7;
	rms[ROOM10].exits_to_room[EAST] = NONE;
	rms[ROOM10].exits_to_room[SOUTH] = NONE;
	rms[ROOM10].exits_to_room[WEST] = NONE;
}*/